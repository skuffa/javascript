// ==UserScript==
// @name          Metaformatter
// @author        SDK
// @description   Initial Release
// @include       http://www.metafilter.com/*
// @include       http://*.metafilter.com/*
// ==/UserScript==

(function() {
  var comments = document.getElementsByClassName("comments");

  for (var i = 0; i < comments.length; i++) {

    var itm = comments[i].getElementsByClassName("smallcopy")[0];
    var cln = itm.cloneNode(true);

    var last = cln.childNodes.length - 1
    while (cln.childNodes[last].nodeValue != ' at ') {
      cln.removeChild(cln.childNodes[last]);
      last = cln.childNodes.length - 1
    }
    cln.removeChild(cln.childNodes[last]);

    while (itm.childNodes[0].nodeValue != ' at ') {
      itm.removeChild(itm.childNodes[0]);
    }
    itm.removeChild(itm.childNodes[0]);

    comments[i].insertBefore(cln, comments[i].firstChild);
  };

})()
